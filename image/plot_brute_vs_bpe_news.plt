set term png
set output 'output/tables_and_plots/brute_vs_bpe_news_en_de.png'
set xlabel 'steps'
set ylabel 'BLEU score'
plot 'output/datasets/EN-DE/Europarl-Brute/mean_news.txt' with lines title "RAW en-de (news)", 'output/datasets/EN-DE/Europarl-BPE/mean_news.txt' with lines title "BPE en-de (news)"
