set term png
set output 'output/tables_and_plots/bpe_en_de.png'
set xlabel 'steps'
set ylabel 'BLEU score'
plot 'output/datasets/EN-DE/Europarl-BPE/mean_test.txt' with lines title "BPE en-de (test)", 'output/datasets/EN-DE/Europarl-BPE/mean_news.txt' with lines title "BPE en-de (news)"
